#
# Wemos D1| ESP8266 Pin |	Functions
# --------|-------------|---------------
# D0	| 16          | GPIO
# D1	| 5           | GPIO, I2C SCL
# D2	| 4           | GPIO, I2C SDA
# D3	| 0           | GPIO
# D4	| 2           | GPIO
# D5	| 14          | GPIO, SPI SCK (Serial Clock)
# D6	| 12          | GPIO, SPI MISO (Master in, Slave out)
# D7	| 13          | GPIO, SPI MOSI (Master out, Slave in)
# D8	| 15          | GPIO, SPI SS (Slave select)
# A0	| A0          | Analog in, via ADC
# RX	| 3           | Receive
# TX	| 1           | Transmit

D0 = 16
D1 = 5
D2 = 4
D3 = 0
D4 = 2
D5 = 14
D6 = 12
D7 = 13
D8 = 15
A0 = 'A0'
RX = 3
TX = 1

