import uos
import gc


def memory_report():
    """ Print total and free system memory """
    fs_stat = uos.statfs('/')
    fs_size = fs_stat[0]  * fs_stat[2]
    fs_free = fs_stat[0] * fs_stat[3]
    print("Memory Report | Total: {} KB | Available: {} KB ({}%)".format(
        fs_size / 1024,
        fs_free / 1024,
        (fs_free / fs_size) * 100
    ) 


def available_ram():
    """ Returns available memory in kilobytes """
    return gc.mem_free() / 1024


