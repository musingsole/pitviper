from HTTPServer import endpoint_handlers, set_wlan_to_access_point
from HTTPServer import http_daemon, success, failure
from machine import reset
from time import sleep
from Motor import motor_controller

print("Ready for operation!")


def get_control_page():
    with open("control.html", "r") as f:
        page = f.read()
    return page


@endpoint_handlers.add_endpoint("/")
def motor_control(request):
    print("Motor Control Received:\n{}".format(request))
    if 'query_parameters' in request and 'motor_value' in request['query_parameters']:
        print("Received: {}".format(request['body']))
        mv = int(request['query_parameters'].split("&")[0].split("=")[1])
        motor_controller.duty(mv)

    page = get_control_page()
    page = page.replace("{message}", "")
    page = page.replace("{motor_value}", str(motor_controller.duty()))

    return page 

print("Starting pitviper Access Point")
set_wlan_to_access_point()
print("Starting HTTP Daemon")
http_daemon()

