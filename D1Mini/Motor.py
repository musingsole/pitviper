from machine import PWM, Pin


class MotorController:
    def __init__(self):
        self.pwm = PWM(Pin(2))
        self.pwm.duty(1023)

    def duty(self, duty=None):
        if duty is None:
            return self.pwm.duty()

        duty = int(duty)
        duty = min(1023, duty)
        duty = max(0, duty)
        self.pwm.duty(duty)

    def __repr__(self):
        return str(self.pwm)

motor_controller = MotorController()

